<?php
/**
 * Created by PhpStorm.
 * User: Rico Bakels
 * Date: 29-9-2016
 * Time: 17:00
 */

namespace SearchBundle\Resources\service;


 use Doctrine\Common\Collections\ArrayCollection;
 use SearchBundle\Entity\Movie;

 class Search {

    public function containsMovie(Movie $data) {
        $format = 'http://www.omdbapi.com/?s=%s&y=%s&r=json';
        $link = sprintf($format, $data->getTitle(), $data->getYear());
        $jsonResponse = json_decode(file_get_contents($link), true);

        if (array_key_exists('Error', $jsonResponse)) {
            return false;
        }
        return true;
    }

    public function generateMovieList(Movie $data) {
        $movies = new ArrayCollection();
        $format = 'http://www.omdbapi.com/?s=%s&y=%s&r=json';
        $link = sprintf($format, $data->getTitle(), $data->getYear());
        $jsonResponse = json_decode(file_get_contents($link), true);

        if (array_key_exists('Error', $jsonResponse)) {
            return $jsonResponse;
        }

        $total = $jsonResponse['totalResults'];

        for ($i = 1; $movies->count() < $total; $i++) {
            $format = 'http://www.omdbapi.com/?s=%s&y=%s&page=%d&r=json';
            $link = sprintf($format, $data->getTitle(), $data->getYear(), $i);
            $response = json_decode(file_get_contents($link), true);
//            dump($response); exit();

            foreach ($response['Search'] as $m) {
                $movies->add($m);
            }
        }

        return ($movies);
    }

}