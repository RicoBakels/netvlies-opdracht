<?php

namespace SearchBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use SearchBundle\Entity\Movie;
use SearchBundle\Form\SearchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use SearchBundle\Resources\service\Search;
use AshleyDawson\SimplePagination\Paginator;

class DefaultController extends Controller {

    public function searchAction() {
        $search = $this->get('search.movie');
        $paginator = new Paginator();
        $paginator->setItemsPerPage(10);

        $movie = new Movie();
        $form = $this->createForm(SearchType::class, $movie);
        $request = Request::createFromGlobals();
        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($search->containsMovie($form->getData())) {

                $movies = $search->generateMovieList($form->getData());

                $paginator->setItemTotalCallback(function () use ($movies) {
                    return count($movies);
                });

                $paginator->setSliceCallback(function ($offset, $length) use ($movies) {
                    return array_slice($movies->toArray(), $offset, $length);
                });

                $pagination = $paginator->paginate((int)$request->query->get('page', 1));



                return $this->render('@Search/Default/movieList.html.twig', array(
                    'pagination' => $pagination,
                ));
            } else {
                return $this->render('@Search/Default/search.html.twig', array(
                    'form' => $form->createView(),
                    'error' => 'Geen films gevonden!'
                ));
            }
        }
            return $this->render('@Search/Default/search.html.twig', array(
                'form' => $form->createView()
            ));
    }

    public function showMovieListAtion(Form $form, Request $request){

    }

    public function showMovieDetailAction($movieID) {
        $link = sprintf('http://www.omdbapi.com/?i=%s&r=json', $movieID);
//        dump($link);exit();

        $movie = json_decode(file_get_contents($link), true);

//        dump($movie); exit();
        return $this->render('@Search/Default/movieDetail.html.twig', array(
            'movie' => $movie,
        ));
    }
}
